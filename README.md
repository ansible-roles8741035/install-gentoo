Role Name
=========

This role aims to provide a fully automated Gentoo Linux installation.

Requirements
------------

- Live boot on any Linux live image Gentoo image.
- Enable ssh on live system.
- Set a root password.
- Write down the IP address of your live system
- Write down the disk name on which you want to install Gentoo

**Install on nvme drives is actually not supported**.

Role Variables
--------------

Explain each variable setting here.

Dependencies
------------

- community.general
- ansible.posix

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:
```yaml
---
- hosts: all
  remote_user: root
  gather_facts: no
  vars:
    disk_name: "/dev/vda"
    disk_boot_size: "1GiB"
    disk_root_size: "20g"
    disk_var_size: "100g"
    disk_home_size: "80g"
    stage3_url: "http://gentoo.mirrors.ovh.net/gentoo-distfiles/releases/amd64/autobuilds/20230730T170144Z/stage3-amd64-openrc-20230730T170144Z.tar.xz"
    net_subnet: "192.168.1.0/24"
    net_hostname: "gentoo-01"
    net_ip: "192.168.1.5/24"
    net_gateway: "192.168.1.1"
    net_dns: "1.1.1.1"
  roles:
    - install-gentoo
```
To run the playbook
```bash
# Assuming your live Gentoo got IP 192.168.1.20
ansible-playbook -k -i ,192.168.1.20 install-gentoo.yml
```

License
-------

BSD

Author Information
------------------

Written and maintained by ramius. (gitlab: @RamiusLr)
