---
- name: Check if system is UEFI
  ansible.builtin.stat:
    path: /sys/firmware/efi/efivars
  register: efi_boot

- name: Set is_uefi variable
  ansible.builtin.set_fact:
    is_uefi: "{{ efi_boot.stat.exists }}"

- name: Update disks info for the kernel
  ansible.builtin.shell: "partprobe {{ disk_name }}"

- name: Get disks info
  community.general.parted:
    device: "{{ disk_name }}"
    unit: MiB
  register: disk_info

- name: Unmount all mounted partitions from target disk
  ansible.posix.mount:
    path: /mnt/gentoo
    state: unmounted

- name: Remove all partitions from disk
  community.general.parted:
    device: "{{ disk_name }}"
    number: '{{ item.num }}'
    state: absent
  loop: '{{ disk_info.partitions }}'

- name: Create EFI partition (UEFI)
  community.general.parted:
    device: "{{ disk_name }}"
    label: gpt
    number: 1
    state: present
    flags: [ esp ]
    part_start: 1MiB
    part_end: 100MiB
  when: is_uefi == True

- name: Create boot partition (UEFI)
  community.general.parted:
    device: "{{ disk_name }}"
    label: gpt
    number: 2
    state: present
    part_start: 101MiB
    part_end: "{{ disk_boot_size }}"
  when: is_uefi == True

- name: Create boot partition (BIOS)
  community.general.parted:
    device: "{{ disk_name }}"
    label: msdos
    number: 1
    state: present
    part_start: 1MiB
    part_end: "{{ disk_boot_size }}"
  when: is_uefi == False

- name: Update disks info
  community.general.parted:
    device: "{{ disk_name }}"
    unit: MiB
  register: disk_info

- name: Create main LVM partition (UEFI)
  community.general.parted:
    device: "{{ disk_name }}"
    label: gpt
    number: "{{ (disk_info.partitions|sort(attribute='num')|last).num +1 }}"
    flags: [ lvm ]
    state: present
    part_start: "{{ (disk_info.partitions|sort(attribute='num')|last).end +1 }}MiB"
    part_end: "100%"
  when: is_uefi == True

- name: Create main LVM partition (BIOS)
  community.general.parted:
    device: "{{ disk_name }}"
    label: msdos
    number: "{{ (disk_info.partitions|sort(attribute='num')|last).num +1 }}"
    flags: [ lvm ]
    state: present
    part_start: "{{ (disk_info.partitions|sort(attribute='num')|last).end +1 }}MiB"
    part_end: "100%"
  when: is_uefi == False

- name: Create LVM PV and VG
  community.general.lvg:
    vg: rootvg
    pvs: "{{ disk_name }}{{ (disk_info.partitions|sort(attribute='num')|last).num +1 }}"
    pvresize: true

- name: Create LVM root
  community.general.lvol:
    vg: rootvg
    lv: rootlv
    size: "{{ disk_root_size }}"

- name: Format EFI partition (UEFI)
  community.general.filesystem:
    device: "{{ disk_name }}1"
    fstype: vfat
    force: true
  when: is_uefi == True

- name: Format boot partition (UEFI)
  community.general.filesystem:
    device: "{{ disk_name }}2"
    fstype: ext4
    force: true
  when: is_uefi == True

- name: Format boot partition (BIOS)
  community.general.filesystem:
    device: "{{ disk_name }}1"
    fstype: ext4
    force: true
  when: is_uefi == False

- name: Format LVM root
  community.general.filesystem:
    device: /dev/rootvg/rootlv
    fstype: ext4
    force: true

- name: Create LVM var
  community.general.lvol:
    vg: rootvg
    lv: varlv
    size: "{{ disk_var_size }}"

- name: Format LVM var
  community.general.filesystem:
    device: /dev/rootvg/varlv
    fstype: ext4
    force: true

- name: Create LVM home
  community.general.lvol:
    vg: rootvg
    lv: homelv
    size: "{{ disk_home_size }}"

- name: Format LVM home
  community.general.filesystem:
    device: /dev/rootvg/homelv
    fstype: ext4
    force: true

- name: Mount root into /mnt/gentoo
  ansible.posix.mount:
    path: /mnt/gentoo
    src: /dev/rootvg/rootlv
    fstype: ext4
    state: mounted

- name: Create /boot folder
  ansible.builtin.file:
    path: /mnt/gentoo/boot
    state: directory

- name: Mount boot in /mnt/gentoo/boot (BIOS)
  ansible.posix.mount:
    path: /mnt/gentoo/boot
    src: "{{ disk_name }}1"
    fstype: ext4
    state: mounted
  when: is_uefi == False

- name: Mount boot in /mnt/gentoo/boot (UEFI)
  ansible.posix.mount:
    path: /mnt/gentoo/boot
    src: "{{ disk_name }}2"
    fstype: ext4
    state: mounted
  when: is_uefi == True

- name: Create /boot/EFI directory
  ansible.builtin.file:
    path: /mnt/gentoo/boot/efi
    state: directory
  when: is_uefi == True

- name: Mount EFI info /mnt/gentoo/boot/efi
  ansible.posix.mount:
    path: /mnt/gentoo/boot/efi
    src: "{{ disk_name }}1"
    fstype: vfat
    state: mounted
  when: is_uefi == True

- name: Create /var and /home directories
  ansible.builtin.file:
    path: "{{ item }}"
    state: directory
  loop:
    - /mnt/gentoo/var
    - /mnt/gentoo/home

- name: Mount var into /mnt/gentoo/var
  ansible.posix.mount:
    path: /mnt/gentoo/var
    src: /dev/rootvg/varlv
    fstype: ext4
    state: mounted

- name: Mount home into /mnt/gentoo/home
  ansible.posix.mount:
    path: /mnt/gentoo/home
    src: /dev/rootvg/homelv
    fstype: ext4
    state: mounted

